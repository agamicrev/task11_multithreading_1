package tasks.task5_sleeps;

/**
 * Task 5. Create a task that sleeps for a random amount of time between 1 and 10 seconds, then displays its
 * sleep time and exits. Create and run a quantity (given on the command line) of these tasks. Do it
 * by using ScheduledThreadPool.
 */
public class App {

  public static void main(String[] args) {
    Sleep sleep = new Sleep();
    sleep.startSleep();
  }

}

package tasks.task5_sleeps;

import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Sleep {

  public void startSleep() {
    ScheduledFuture<?> f;
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(10);
    System.out.println("Input number of tasks:");
    Scanner scanner = new Scanner(System.in);
    int input = scanner.nextInt();
    for (int i = 1; i <= input; i++) {
      String name = "TASK " + i;
      f = executor.schedule(() -> {
        System.out.println(name + " WOKE UP IN " + "THREAD <"
            + Thread.currentThread().getName() + "> AND FINISHED");
      }, Tools.getRandomInt(1, 10), TimeUnit.SECONDS);
      long delay = f.getDelay(TimeUnit.SECONDS);
      System.out.println(name + " DELAY: " + delay + " sec");

    }
    try {
      Tools.await(executor);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("**** ALL TASKS FINISHED ****");
  }
}

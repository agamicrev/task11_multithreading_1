package tasks.task7_pipe_communication;

/**
 * Task 7. Write program in which two tasks use a pipe
 * to communicate.
 */
public class App {

  public static void main(String[] args) {
    Pipe pipe = new Pipe();
    pipe.startPipe();
  }
}

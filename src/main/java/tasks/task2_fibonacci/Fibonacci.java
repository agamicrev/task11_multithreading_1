package tasks.task2_fibonacci;

import java.util.concurrent.TimeUnit;

public class Fibonacci {

  public void startFibonacci() {
    System.out.println("**** Fibonacci Started ****");
    int min = 10;
    int max = 20;

    for (int i = 1; i <= 5; i++) {
      int set = Tools.getRandomInt(min, max);
      long pause = Tools.getRandomLong(1000, 3000);
      String name = "--- THREAD " + i;
      Thread t = new Thread(() -> {
        showFibonacciSecuenceWithPause(name, set, pause);
      });
      t.start();
    }
    try {
      TimeUnit.SECONDS.sleep(3);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("**** Fibonacci Finished ****");
  }

  public static void showFibonacciSecuenceWithPause(String name, int set, Long pause) {
    try {
      Thread.sleep(pause);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println(name + " --- " + new ProduceFibonacci(set).getFibonacciSequence());
  }
}

package tasks.task2_fibonacci;

/**
 * Task 2. Create a task that produces a sequence of n Fibonacci numbers, where n is provided
 * to the constructor of the task. Create a number of these tasks and drive them using
 * threads.
 */
public class App {

  public static void main (String[] args) {
    Fibonacci fibonacci = new Fibonacci();
    fibonacci.startFibonacci();
  }
}

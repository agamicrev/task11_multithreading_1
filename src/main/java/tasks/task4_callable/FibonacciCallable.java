package tasks.task4_callable;

import java.util.List;
import java.util.concurrent.Callable;
import tasks.task2_fibonacci.ProduceFibonacci;

public class FibonacciCallable implements Callable<Long> {

  private String name;

  public FibonacciCallable(String name) {
    this.name = name;
  }

  @Override
  public Long call() {
    int set = Tools.getRandomInt(10, 15);
    List<Long> sequence = new ProduceFibonacci(set).getFibonacciSequence();
    System.out.println(name + " --- " + sequence);
    return getFibonacciSum(sequence);
  }

  private long getFibonacciSum(List<Long> sequence) {
    return sequence.stream().mapToLong(Long::longValue).sum();
  }
}

package tasks.task4_callable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciSum {

  public void startFibonacciSum() {
    ExecutorService service = Executors.newFixedThreadPool(3);
    long sum1 = 0;
    long sum3 = 0;
    long sum2 = 0;
    try {
      sum1 = service.submit(new FibonacciCallable("--- THREAD 1 ")).get();
      sum2 = service.submit(new FibonacciCallable("--- THREAD 2 ")).get();
      sum3 = service.submit(new FibonacciCallable("--- THREAD 3 ")).get();
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
    try {
      Tools.await(service);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("Thread 1 SUM: " + sum1);
    System.out.println("Thread 2 SUM: " + sum2);
    System.out.println("Thread 3 SUM: " + sum3);
  }
}

package tasks.task4_callable;

/**
 * Task 4. Modify Task 2 so that the task is a Callable that sums the values of all the Fibonacci
 * numbers. Create several tasks and display the results.
 */
public class App {

  public static void main(String[] args) {

    FibonacciSum fibonacciSum = new FibonacciSum();
    fibonacciSum.startFibonacciSum();
  }
}

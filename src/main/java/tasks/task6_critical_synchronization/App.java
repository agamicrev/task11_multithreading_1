package tasks.task6_critical_synchronization;

/**
 * Task 6. Create a class with three methods containing critical sections that all synchronize on the same
 * object. Create multiple tasks to demonstrate that only one of these methods can run at a time.
 * Now modify the methods so that each one synchronizes on a different object and show that all
 * three methods can be running at once.
 */
public class App {

  public static void main(String[] args) {
    Synchronization synchronization = new Synchronization();
    synchronization.startSynchronization();
  }
}

package tasks.task1_ping_pong;

public class Game {
  private final Object ball = new Object();

  private void playerOneGame(Player player) {
    synchronized (ball) {
      for (int i = 0; i < 10; i++) {
        try {
          ball.wait();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        play(player);
        ball.notify();
      }
    }
  }

  private void playerTwoGame(Player player) {
    synchronized (ball) {
      for (int i = 0; i < 10; i++) {
        ball.notify();
        try {
          ball.wait();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        play(player);
      }
    }
  }

  private void play(Player player) {
    try {
      Thread.sleep(1200);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    int point = player.tryToScore();
    player.addPoint(point);
    System.out.print(player.getName() + (point != 0 ? " *** SCORES ***" : " misses "));
    System.out.println(" [Points - " + player.getPoints() + "]");
  }

  public void startGame() {
    Player playerOne = new Player("Batman");
    Player playerTwo = new Player("Superman");
    Thread t1 = new Thread(() -> playerOneGame(playerOne));
    Thread t2 = new Thread(() -> playerTwoGame(playerTwo));
    t1.setPriority(10);
    t1.start();
    t2.start();
    try {
      t1.join();
      t2.join();
    } catch (InterruptedException e) {}
    if (playerOne.compareTo(playerTwo) > 0) {
      System.out.println(playerOne.getName() + " IS THE WINNER!");
    } else if (playerOne.compareTo(playerTwo) < 0) {
      System.out.println(playerTwo.getName() + " IS THE WINNER!");
    } else {
      System.out.println("It's a draw");
    }
    System.out.println("<<<<<<< FINAL SCORE " + playerOne.getPoints() + ":" + playerTwo.getPoints() + " >>>>>>>");
  }
}

package tasks.task1_ping_pong;

/**
 * Task 1. Write simple “ping-pong” program using wait() and notify().
 */
public class App {

  public static void main (String[] args) {
    Game game = new Game();
    System.out.println("****** BATMAN VS SUPERMAN ******");
    game.startGame();
  }
}

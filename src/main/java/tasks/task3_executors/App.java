package tasks.task3_executors;

/**
 * Task 3. Repeat task 2 using different types of executors.
 */
public class App {

  public static void main(String[] args) {
   FibonacciExecutors fibonacciExecutors = new FibonacciExecutors();
   fibonacciExecutors.startFibonacciExecutors();
  }
}

package tasks.task3_executors;

import static tasks.task2_fibonacci.Fibonacci.showFibonacciSecuenceWithPause;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciExecutors {

  public void startFibonacciExecutors() {
    int min = 10;
    int max = 20;
    ExecutorService executor = Executors.newSingleThreadExecutor();
    executor.execute(() -> {
      int set = Tools.getRandomInt(min, max);
      long pause = Tools.getRandomLong(1000, 3000);
      System.out.println("--- Single Thread Executor --- " + (pause / 1000) + " sec");
      showFibonacciSecuenceWithPause("Single Thread Executor", set, pause);

    });
    ExecutorService threadPoolExecutor = Executors.newFixedThreadPool(3);
    threadPoolExecutor.submit(() -> {
      int set = Tools.getRandomInt(min, max);
      long pause = 10000;
      System.out
          .println("--- New Fixed Thread PoolExecutor --- " + (pause / 1000) + " sec");
      showFibonacciSecuenceWithPause("NewFixedThreadPoolExecutor", set, pause);

    });
    ExecutorService cachedThreadPoolExecutor = Executors.newCachedThreadPool();
    cachedThreadPoolExecutor.submit(() -> {
      int set = Tools.getRandomInt(min, max);
      long pause = Tools.getRandomLong(1000, 5000);
      System.out
          .println("--- New Cached Thread Pool Executor --- " + (pause / 1000) + " sec");
      showFibonacciSecuenceWithPause("NewCachedThreadPoolExecutor", set, pause);

    });
    try {
      Tools.await(executor);
      Tools.await(threadPoolExecutor);
      Tools.await(cachedThreadPoolExecutor);
    } catch (InterruptedException e) {
    }
  }
}
